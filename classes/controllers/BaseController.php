<?php
class BaseController{
	
	protected $model;
	protected $content_view;
	protected $master_page;
	protected $layout = 'master.php';
	
	public function render($view, $model){
		
		$this->content_view = APPLICATION_CONTROLLERS_DIR .'/views/'.$view.'.php';
		$this->model = $model;
		require_once APPLICATION_CONTROLLERS_DIR .'/views/'.$this->layout;		
		
	}
	
}

?>
