<?php
class HomeController extends BaseController{
	
	public function index(){	

		$controller = new HomeController();
		$da = new PostsDA();
		$model = [];
		$model['posts'] = $da->list_lastest();			
		
		parent::render('index', $model);
	}
	
	public function post($id){	

		$controller = new HomeController();
		$da = new PostsDA();
		$post = $da->get($id);
				
		parent::render('post', $post);		
	}	
	
}

?>
