<?php

// Database
require_once(APPLICATION_DAO_DIR.'/db.class.php');
require_once(APPLICATION_DAO_DIR.'/bind.php');

// Include DAO
require_once(APPLICATION_DAO_DIR.'/categories.access.php');
require_once(APPLICATION_DAO_DIR.'/posts.access.php');

// Include models
require_once(APPLICATION_MODEL_DIR.'/category.php');
require_once(APPLICATION_MODEL_DIR.'/post.php');

?>