<?php
class DB {
 
	protected $db_name = 'personal-blog';
	protected $db_user = 'root';
	protected $db_pass = '';
	protected $db_host = 'localhost'; 
	protected $connection;
    
	//open a connection to the database. Make sure this is called
	//on every page that needs to use the database.
	public function connect() {
		$this->connection = new mysqli($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
 
		if($this->connection->connect_error) {
			trigger_error('Cannot connect to database. ' . $this->connection->connect_error);
		}
		return true;
	}
 
	//takes a mysql row set and returns an associative array, where the keys
	//in the array are the column names in the row set. If singleRow is set to
	//true, then it will return a single row instead of an array of rows.
	public function processRowSet($rowSet, $return_object, $singleRow=false)
	{
		$resultArray = array();
		while($row = mysqli_fetch_assoc($rowSet))
		{
			array_push($resultArray, new $return_object($row));
		}
 
		if($singleRow === true)
			return $resultArray[0];
 
		return $resultArray;
	}
 
	//Select rows from the database.
	//returns a full row or rows from $table using $where as the where clause.
	//return value is an associative array with column names as keys.
	public function select($table, $return_object, $where = null) {
		
		$sql = "SELECT * FROM $table";
		if (isset($where))
			$sql .= ' WHERE $where';
		$result = mysqli_query($this->connection, $sql) or die(mysqli_error($this->connection));
		if(mysqli_num_rows($result) == 1)
			return $this->processRowSet($result, $return_object, true);
 
		return $this->processRowSet($result, $return_object);
	}
	
	//Select rows from the database.
	//returns a full row or rows from $table using $where as the where clause.
	//return value is an associative array with column names as keys.
	public function select2($sql, $return_object, $binds = []) {				

		$statement = $this->connection->prepare($sql);
		if($statement === false) {
		  trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $this->connection->errno . ' ' . $this->connection->error, E_USER_ERROR);
		}
	
		$array_binds_values = [];
		$binds_types = "";
		foreach($binds as $bind){	
			$binds_types .= $bind->type;
		}

		$a_params = array();
		 
		/* with call_user_func_array, array params must be passed by reference */
		$a_params[] = & $binds_types;
		
		foreach($binds as $bind){	
			$a_params[] = 1;
		}
		var_dump($sql, $a_params);
		if (count($array_binds_values) > 0){
			//$param = array($binds_types);
			//$param = array($binds_types, &$id, &$type, &$result, &$path);
			//call_user_func_array(array('mysqli_stmt_bind_param'), $param);
			call_user_func_array(array($statement, 'bind_param'), $a_params);
			//mysqli_stmt_bind_param($statement, $binds_types, $array_binds_values);
			//$teste = 2;
			//mysqli_stmt_bind_param($statement, 'd', $teste);
		}
					
		//mysqli_stmt_execute($statement) or die(mysqli_error($this->connection));
		$statement->execute();

		var_dump($statement);

		//$result = mysqli_stmt_get_result($statement);
		$result = $statement->get_result();

		var_dump($result);
		//$result = mysqli_query($this->connection, $sql) or die(mysqli_error($this->connection));
		//if(mysqli_num_rows($result) == 1)
		//	return $this->processRowSet($result, $return_object, true);
	
		//return $this->processRowSet($result, $return_object);		
		return array();
	}
 
	//Updates a current row in the database.
	//takes an array of data, where the keys in the array are the column names
	//and the values are the data that will be inserted into those columns.
	//$table is the name of the table and $where is the sql where clause.
	public function update($data, $table, $where) {
		foreach ($data as $column => $value) {
			$sql = "UPDATE $table SET $column = $value WHERE $where";
			mysqli_query($this->connection, $sql) or die(mysqli_error($this->connection));
		}
		return true;
	}
 
	//Inserts a new row into the database.
	//takes an array of data, where the keys in the array are the column names
	//and the values are the data that will be inserted into those columns.
	//$table is the name of the table.
	public function insert($data, $table) {
 
		$columns = "";
		$values = "";
 
		foreach ($data as $column => $value) {
			$columns .= ($columns == "") ? "" : ", ";
			$columns .= $column;
			$values .= ($values == "") ? "" : ", ";
			$values .= $value;
		}
 
		$sql = "insert into $table ($columns) values ($values)";
 
		mysqli_query($this->connection, $sql) or die(mysqli_error($this->connection));
 
		//return the ID of the user in the database.
		return mysqli_insert_id();
 
	}
 
}
?>