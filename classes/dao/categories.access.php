<?php
class CategoriesDA {
	
	public function save($object, $is_new = false) {
		//create a new database object.
		$db = new DB();
		
		//if the user is already registered and we're
		//just updating their info.
		if(!$is_new) {
			//set the data array
			$data = array(
				"description" => "'$object->description'",
				"deleted" => "'$object->deleted'",
			);
			
			//update the row in the database
			$db->update($data, 'categories', 'id = '.$object->id);
		}else {
		//if the user is being registered for the first time.
			$data = array(
				"description" => "'$object->description'",
				"deleted" => "'$object->deleted'",
				"date_created" => "'".date("Y-m-d H:i:s",time())."'"
			);
			
			$object->id = $db->insert($data, 'categories');
			$object->date_created = time();
		}
		return true;
	}
	
	public function get($id) {
		//create a new database object.
		$db = new DB();		
		$db->connect();

		return $db->select2('SELECT * FROM `categories` WHERE id = ? ', 'Category', array(Bind::create('d', $id)));
	}
	
	public function list_all() {
		//create a new database object.
		$db = new DB();
		
		$db->connect();
		
		return $db->select('categories', 'Category');
	}
	
}
?>