<?php
class PostsDA {
	
	public function get($id) {
		//create a new database object.
		$db = new DB();		
		$db->connect();
		
		return $db->select2('SELECT * FROM `posts` where id = ?', 'Post', array(Bind::create('d', $id)));
	}
	

	public function list_lastest() {
		//create a new database object.
		$db = new DB();		
		$db->connect();
		
		return $db->select2('SELECT * FROM `posts` order by date_created DESC LIMIT 0,3', 'Post');
	}
	
	public function list_posts_monthly() {
		//create a new database object.
		$db = new DB();		
		$db->connect();
		
		$return = $db->select2('SELECT DATE_FORMAT(date_created, "%M %Y") MONTH FROM posts group by DATE_FORMAT(date_created, "%Y %m"), DATE_FORMAT(date_created, "%M %Y") ORDER BY DATE_FORMAT(date_created, "%Y %m") LIMIT 0,3', 'Post');
		
		$da = new CategoriesDA();
		foreach($return as $post){
			$post->category = $da->get($post->category_id);
		}
		return $return;
		
	}

	public function get_featured() {
		//create a new database object.
		$db = new DB();		
		$db->connect();
		
		$return = $db->select2('SELECT * FROM `posts` where featured = ? order by date_created DESC LIMIT 0,3', 'Post', array(Bind::create('i', 1)));
		
		$da = new CategoriesDA();
		foreach($return as $post){
			$post->category = $da->get($post->category_id);
		}
		return $return;
		
	}
	
}
?>