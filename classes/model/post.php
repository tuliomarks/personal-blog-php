<?php 
class Post {

	public $id;
	public $title;
	public $article_html;
	public $date_created;
	public $deleted;
	public $featured;	
	public $excerpt;
	public $category_id;	
	
	public $category;

	//Constructor is called whenever a new object is created.
	//Takes an associative array with the DB row as an argument.
	function __construct($data) {
		$this->id = (isset($data['id'])) ? $data['id'] : "";
		$this->title = (isset($data['title'])) ? $data['title'] : "";
		$this->article_html = (isset($data['article_html'])) ? $data['article_html'] : "";
		$this->date_created = (isset($data['date_created'])) ? $data['date_created'] : "";
		$this->deleted = (isset($data['deleted'])) ? $data['deleted'] : "";
		$this->featured = (isset($data['featured'])) ? $data['featured'] : "";
		$this->excerpt = (isset($data['excerpt'])) ? $data['excerpt'] : "";
		$this->category_id = (isset($data['category_id'])) ? $data['category_id'] : "";
		$this->category = (isset($data['category'])) ? $data['category'] : "";
	}
	
}
?>