<?php 
class Category {

	public $id;
	public $description;
	public $dateCreated;
	public $deleted;

	//Constructor is called whenever a new object is created.
	//Takes an associative array with the DB row as an argument.
	function __construct($data) {
		$this->id = (isset($data['id'])) ? $data['id'] : "";
		$this->description = (isset($data['description'])) ? $data['description'] : "";
		$this->date_created = (isset($data['date_created'])) ? $data['date_created'] : "";
		$this->deleted = (isset($data['deleted'])) ? $data['deleted'] : "";
	}
	
}
?>