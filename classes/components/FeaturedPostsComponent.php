<?php

class FeaturedPostsComponent extends BaseComponent {
		
	public function init(){	

		$da = new PostsDA();		
		$posts = $da->get_featured();

		parent::render('featured_posts', $posts);		
	}
	
}

?>