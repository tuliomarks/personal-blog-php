<?php
class BaseComponent{
		
	protected $model;
	protected $content_view;
	protected $master_page;
	
	public function render($view, $model){
		
		$this->content_view = $view.'.php';
		$this->model = $model;
		require_once APPLICATION_COMPONENTS_DIR .'/views/'. $this->content_view;		
		
	}
	
}
?>