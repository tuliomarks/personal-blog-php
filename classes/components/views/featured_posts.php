<? if(count($this->model)>0){ ?>
<div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
    <div class="col-md-6 px-0">
        <h1 class="display-4 font-italic"><?echo htmlentities($this->model[0]->title)?></h1>
        <p class="lead my-3"><?echo htmlentities($this->model[0]->excerpt)?></p>
        <p class="lead mb-0"><a href="/post/<?echo $this->model[0]->id?>" class="text-white font-weight-bold">Continue reading...</a></p>
    </div>
</div>
<div class="row mb-2">
<? if(count($this->model)>1){ ?>
    <div class="col-md-6">
        <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
                <strong class="d-inline-block mb-2 text-success"><? echo htmlentities($model[1]->category->description); ?></strong>
                <h3 class="mb-0">
                <a class="text-dark" href="#"><?echo htmlentities($this->model[1]->title)?></a>
                </h3>
                <div class="mb-1 text-muted"><? echo date("F j", strtotime($model[1]->date_created)) ?></div>
                <p class="card-text mb-auto"><?echo htmlentities($this->model[1]->excerpt)?></p>
                <a href="/post/<?echo htmlentities($this->model[1]->id)?>">Continue reading</a>
            </div>
            <img class="card-img-right flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="Thumbnail [200x250]" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22250%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20250%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1621622489f%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A13pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1621622489f%22%3E%3Crect%20width%3D%22200%22%20height%3D%22250%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2256.203125%22%20y%3D%22131%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true" style="width: 200px; height: 250px;">
        </div>
    </div>
    <? } 
    if(count($this->model)>2){ ?>
    <div class="col-md-6">
        <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
                <strong class="d-inline-block mb-2 text-success"><? echo htmlentities($model[2]->category->description) ?></strong>
                <h3 class="mb-0">
                <a class="text-dark" href="#"><?echo htmlentities($this->model[2]->title)?></a>
                </h3>
                <div class="mb-1 text-muted"><? echo date("F j", strtotime($model[2]->date_created)) ?></div>
                <p class="card-text mb-auto"><?echo htmlentities($this->model[2]->excerpt)?></p>
                <a href="/post/<?echo htmlentities($this->model[2]->id)?>">Continue reading</a>
            </div>
            <img class="card-img-right flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="Thumbnail [200x250]" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22250%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20250%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1621622489f%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A13pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1621622489f%22%3E%3Crect%20width%3D%22200%22%20height%3D%22250%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2256.203125%22%20y%3D%22131%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true" style="width: 200px; height: 250px;">
        </div>
    </div>
<? } ?>
</div>  
<? } ?>