<?php

DEFINE('APPLICATION_DAO_DIR', dirname(dirname(__FILE__)) .'/classes/dao');
DEFINE('APPLICATION_MODEL_DIR',dirname(dirname(__FILE__)) .'/classes/model');
DEFINE('APPLICATION_CONTROLLERS_DIR',dirname(dirname(__FILE__)) .'/classes/controllers');
DEFINE('APPLICATION_COMPONENTS_DIR',dirname(dirname(__FILE__)) .'/classes/components');

//DataBase Configuration
require_once(APPLICATION_DAO_DIR.'/config.php');
//Controllers
require_once(APPLICATION_CONTROLLERS_DIR.'/config.php');
//Components
require_once(APPLICATION_COMPONENTS_DIR.'/config.php');

//Application Configuration
DEFINE('APPLICATION_TITLE','THE AWESOME! Blog');

//Router Configuration
require_once(dirname(dirname(__FILE__)) . '/conf/route.php');
require_once(dirname(dirname(__FILE__)) . '/conf/router.php');

Router::get(new Route('/', 'HomeController@index'));
Router::get(new Route('/post', 'HomeController@post'));
Router::run();

?>