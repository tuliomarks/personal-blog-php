<?php

class Router
{
	const GET = 'GET';
	const POST = 'POST';

	private static $_routes = array(
		'GET' => array(),
		'POST' => array()
	);

	public static function add(Route $route, $method)
	{
		switch ($method) {
			case 'GET':
				self::$_routes['GET'][$route->getURL()] = $route;
			break;

			case 'POST':
				self::$_routes['POST'][$route->getURL()] = $route;
			break;

			default:
				exit('Error!');
			break;
		}
	}

	public static function get(Route $route)
	{
		self::add($route, self::GET);
	}

	public static function post(Route $route)
	{
		self::add($route, self::POST);
	}

	public static function run()
	{	
		$path = strtok($_SERVER["REQUEST_URI"],'?');
		foreach (self::$_routes[$_SERVER['REQUEST_METHOD']] as $url => $route) {			
			if (preg_match($url, $path, $matches)) {	
				
				$callback_args = [];
				foreach( explode('&', $_SERVER['QUERY_STRING']) as $param){
					if (count(explode('=', $param))<=1) 
						continue;
					$callback_args[] = explode('=', $param)[1];
				}
				call_user_func_array($route->getCallback(), $callback_args);
				return;
			}
		}
	}
}