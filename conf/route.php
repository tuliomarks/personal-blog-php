<?php 
class Route
{
	private $_url;
	private $_controller;
	private $_controller_name;
	private $_action;

	public function __construct($_url, $callback)
	{
		$this->_setURL($_url);

		try
		{
			$this->_setCallback($callback);
		}
		catch(InvalidArgumentException $e)
		{
		   exit($e);
		}
	}

	protected function _setCallback($callback)
	{
		$callback = (string) $callback;
		$aCallback = explode('@', $callback);
		
		if (is_callable($callback)) {
			$this->_controller = $callback;
		}
		else if (count($aCallback) == 2 && file_exists(APPLICATION_CONTROLLERS_DIR . '/' . $aCallback[0] . '.php'))
		{			
			require_once APPLICATION_CONTROLLERS_DIR . '/' . $aCallback[0] . '.php'; // instead of including the class you can make usage of http://php.net/manual/en/function.spl-autoload-register.php
			$this->_controller = new $aCallback[0];
			$this->_controller_name = $aCallback[0];
			$this->_action = $aCallback[1];
			
		}
		else
		{
			throw new InvalidArgumentException('$callback is invalid.');
		}
	}

	protected function _setURL($url)
	{
		$this->_url = '/^' . str_replace('/', '\\/', $url) . '$/';
	}

	public function getURL()
	{
		return $this->_url;
	}

	public function getCallback()
	{
		return array($this->_controller, $this->_action);
	}
}
?>